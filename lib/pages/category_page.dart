import 'package:creonit_test_app/application/blocs/category_bloc/category_page_bloc.dart';
import 'package:creonit_test_app/application/core/MixinColor.dart';
import 'package:creonit_test_app/infrastructure/category/category_repository.dart';
import 'package:creonit_test_app/infrastructure/core/category/category_local_datasource.dart';
import 'package:creonit_test_app/infrastructure/core/category/category_remote_datasource.dart';
import 'package:creonit_test_app/infrastructure/core/network_info.dart';
import 'package:creonit_test_app/pages/product_page.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hive/hive.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class CategoryPage extends StatelessWidget with MixinColor {
  const CategoryPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        title: Text(
          'Категории',
          style: TextStyle(
              color: greyColor(), fontWeight: FontWeight.w600, fontSize: 16),
        ),
        centerTitle: true,
        leading: SizedBox(
          width: 5.w,
          height: 11.h,
          child: InkWell(
              child: Center(
                  child: SvgPicture.asset('assets/icons/back_arrow.svg'))),
        ),
      ),
      body: BlocProvider(
        create: (_) => CategoryPageBloc(
            categoryRepository: CategoryRepository(
                categoryLocalDatasource:
                    CategoryLocalDatasource(box: Hive.box('category')),
                networkInfo: NetworkInfo(InternetConnectionChecker()),
                categoryRemoteDatasource:
                    CategoryRemoteDataSource(client: Dio())))
          ..add(const CategoryPageEvent.getCategories()),
        child: Builder(
          builder: (context) =>
              BlocBuilder<CategoryPageBloc, CategoryPageState>(
            builder: (context, state) => state.map(
              failureState: (value) {
                return const Center(child: Text('All FAILED'));
              },
              initialState: (value) {
                return const Center(
                  child: CircularProgressIndicator(),
                );
              },
              stateWithCategoryList: (value) {
                return Column(
                  children: [
                    Divider(
                      color: dividerColor(),
                      thickness: .5.h,
                    ),
                    Flexible(
                      child: ListView.separated(
                        physics: const NeverScrollableScrollPhysics(),
                        scrollDirection: Axis.vertical,
                        itemCount: value.categories.length,
                        separatorBuilder: (context, index) => Padding(
                          padding: EdgeInsets.symmetric(horizontal: 16.w),
                          child: Divider(
                            color: dividerColor(),
                            thickness: .5.h,
                          ),
                        ),
                        itemBuilder: (context, index) {
                          return GestureDetector(
                            behavior: HitTestBehavior.translucent,
                            onTap: () => Navigator.of(context).push(
                              MaterialPageRoute(
                                builder: (context) => ProductPage(
                                    id: value.categories.elementAt(index).id,
                                    label: value.categories
                                        .elementAt(index)
                                        .title),
                              ),
                            ),
                            child: Row(
                              children: [
                                Padding(
                                  padding: EdgeInsets.only(
                                      left: 16.w, top: 16.h, bottom: 12.h),
                                  child: Text(
                                    value.categories.elementAt(index).title,
                                    style: TextStyle(
                                      fontSize: 16,
                                      color: greyColor(),
                                      fontWeight: FontWeight.w600,
                                    ),
                                  ),
                                ),
                                const Spacer(),
                                Padding(
                                  padding: EdgeInsets.only(right: 22.w),
                                  child: SvgPicture.asset(
                                    'assets/icons/category_arrow.svg',
                                    width: 8.w,
                                    height: 12.h,
                                  ),
                                ),
                              ],
                            ),
                          );
                        },
                      ),
                    ),
                  ],
                );
              },
            ),
          ),
        ),
      ),
    );
  }
}
