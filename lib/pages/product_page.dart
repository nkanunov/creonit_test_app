import 'package:cached_network_image/cached_network_image.dart';
import 'package:creonit_test_app/application/blocs/product_bloc/product_page_bloc.dart';
import 'package:creonit_test_app/application/core/MixinColor.dart';
import 'package:creonit_test_app/domain/product/entities/product.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ProductPage extends StatefulWidget {
  ProductPage({Key? key, required String label, required int id})
      : _label = label,
        _id = id,
        super(key: key);

  final String _label;
  final int _id;

  @override
  State<ProductPage> createState() => _ProductPageState();
}

class _ProductPageState extends State<ProductPage> with MixinColor {
  ScrollController? _controller;
  int _page = 1;
  final int _limit = 10;
  int _pageCount = 0;
  List<bool> _isProductAddedToCast = [];
  List<Product> _products = [];

  @override
  void initState() {
    super.initState();
    _controller = ScrollController();
    context.read<ProductPageBloc>().add(ProductPageEvent.getProducts(
        index: widget._id,
        page: _page,
        limit: _limit,
        isProductAddedToCast: _isProductAddedToCast,
        isFirst: true));

    _controller?.addListener(() {
      if (_controller?.position.pixels ==
          _controller?.position.maxScrollExtent) {
        if (_page < _pageCount) {
          _page++;
          context.read<ProductPageBloc>().add(ProductPageEvent.getProducts(
              index: widget._id,
              page: _page,
              limit: _limit,
              isProductAddedToCast: _isProductAddedToCast,
              isFirst: false));
        }
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
    _controller?.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        context.read<ProductPageBloc>().add(
              ProductPageEvent.clearData(
                  products: _products,
                  isProductAddedToCast: _isProductAddedToCast),
            );
        return true;
      },
      child: Scaffold(
        appBar: AppBar(
          elevation: 0,
          title: Text(
            widget._label,
            style: TextStyle(
                color: greyColor(), fontWeight: FontWeight.w600, fontSize: 16),
          ),
          centerTitle: true,
          leading: SizedBox(
            width: 11.w,
            height: 20.h,
            child: InkWell(
              onTap: () {
                context.read<ProductPageBloc>().add(
                      ProductPageEvent.clearData(
                          products: _products,
                          isProductAddedToCast: _isProductAddedToCast),
                    );
                Navigator.pop(context);
              },
              child: Center(
                  child: SvgPicture.asset(
                'assets/icons/back_arrow.svg',
              )),
            ),
          ),
        ),
        body: BlocBuilder<ProductPageBloc, ProductPageState>(
          builder: (context, state) => state.map(
            failureState: (value) {
              return const Center(child: Text('Error'));
            },
            initialState: (value) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            },
            stateWithProductList: (value) {
              _pageCount = value.pageCount;
              _isProductAddedToCast = value.isProductAddedToCast;
              _products = value.products;
              return Column(
                children: [
                  Divider(
                    color: greyColor().withOpacity(0.24),
                    thickness: .5.h,
                  ),
                  Expanded(
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 16.w),
                      child: GridView.builder(
                        controller: _controller,
                        physics: const AlwaysScrollableScrollPhysics(),
                        itemCount: value.products.length,
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                            childAspectRatio: 3 / 5,
                            crossAxisSpacing: 15.w,
                            mainAxisSpacing: 52.h,
                            crossAxisCount: 2),
                        itemBuilder: (context, index) => Column(
                          children: [
                            Stack(
                              children: [
                                CachedNetworkImage(
                                  fit: BoxFit.contain,
                                  imageUrl: value.products
                                      .elementAt(index)
                                      .image
                                      .file
                                      .url,
                                  height: 164.h,
                                  width: 164.w,
                                ),
                                Align(
                                  alignment: Alignment.topRight,
                                  child: SvgPicture.asset(
                                      'assets/icons/heart.svg'),
                                )
                              ],
                            ),
                            Padding(
                              padding: EdgeInsets.only(top: 12.h),
                              child: Container(
                                width: 164.w,
                                height: 45.h,
                                decoration: BoxDecoration(
                                    color: productTitleBackgroundColor(),
                                    borderRadius: const BorderRadius.all(
                                        Radius.circular(3))),
                                child: Row(
                                  children: [
                                    Padding(
                                      padding: EdgeInsets.only(left: 8.w),
                                      child: Text(
                                        '${value.products.elementAt(index).price} ₽',
                                        style: TextStyle(
                                            fontWeight: FontWeight.w700,
                                            fontSize: 16,
                                            color: greyColor()),
                                      ),
                                    ),
                                    const Spacer(),
                                    Padding(
                                      padding: EdgeInsets.only(right: 12.w),
                                      child: GestureDetector(
                                        onTap: () {
                                          setState(() {});
                                          context.read<ProductPageBloc>().add(
                                                ProductPageEvent
                                                    .addProductToCard(
                                                  pageCount: value.pageCount,
                                                  id: value.products
                                                      .elementAt(index)
                                                      .id,
                                                  quantity: 1,
                                                  index: index,
                                                  isProductAddedToCast: value
                                                      .isProductAddedToCast,
                                                  products: value.products,
                                                ),
                                              );
                                        },
                                        child: SvgPicture.asset(
                                          'assets/icons/cart.svg',
                                          color: value.isProductAddedToCast
                                                      .elementAt(index) ==
                                                  true
                                              ? activeStateCardButtonColor()
                                              : greyColor(),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Text(
                              value.products.elementAt(index).title,
                              style: TextStyle(
                                color: greyColor(),
                                fontSize: 12,
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              );
            },
          ),
        ),
      ),
    );
  }
}
