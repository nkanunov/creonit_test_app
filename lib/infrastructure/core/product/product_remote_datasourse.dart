import 'package:creonit_test_app/domain/category/category_failure.dart';
import 'package:creonit_test_app/infrastructure/product/product_model.dart';
import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:shared_preferences/shared_preferences.dart';

const categoryURL = 'https://vue-study.skillbox.cc/api/products/';
const accessKeyURL = 'https://vue-study.skillbox.cc/api/users/accessKey';
const basketURL = 'https://vue-study.skillbox.cc/api/baskets/products';
const getBasketURL = 'https://vue-study.skillbox.cc/api/baskets';

abstract class IProductsRemoteDataSource {
  Future<Tuple2<List<ProductModel>, dynamic>> getProductsFromServer(
      {required int index, required int page, int? limit});

  Future<List<ProductModel>> getCartFromServer(String userAccessKey);

  Future<void> addProductToCard(int index, int quantity);
}

class ProductsRemoteDataSource implements IProductsRemoteDataSource {
  ProductsRemoteDataSource({required Dio client}) : _client = client;

  final Dio _client;

  @override
  Future<Tuple2<List<ProductModel>, dynamic>> getProductsFromServer(
      {required int index, required int page, int? limit}) async {
    try {
      final result = await _client
          .get('$categoryURL?categoryId=$index&page=$page&limit=$limit');

      var list = List<ProductModel>.from(
          (result.data['items']).map((e) => ProductModel.fromJson(e)));

      var resultTuple = Tuple2<List<ProductModel>, dynamic>(
          list, result.data['pagination']['pages']);

      return resultTuple;
    } on Exception catch (_) {
      throw const CategoryFailure.serverFailure();
    }
  }

  @override
  Future<void> addProductToCard(int index, int quantity) async {
    final SharedPreferences _preferences =
        await SharedPreferences.getInstance();
    try {
      var accessKey = _preferences.getString('userAccessKey');
      if (accessKey == null) {
        final result = await _client.get(accessKeyURL);
        accessKey = result.data['accessKey'];
        print(accessKey);
        _preferences.setString('userAccessKey', accessKey!);
      }

      var product = {'quantity': quantity, 'productId': index};

      await _client.post("$basketURL?userAccessKey=$accessKey", data: product);
    } on Exception catch (_) {
      throw const CategoryFailure.serverFailure();
    }
  }

  @override
  Future<List<ProductModel>> getCartFromServer(String userAccessKey) async {
    try {
      final result =
          await _client.get('$getBasketURL?userAccessKey=$userAccessKey');

      var list = List<ProductModel>.from((((result.data['items']))
          .map((e) => ProductModel.fromJson(e['product']))));

      return list;
    } on Exception catch (_) {
      throw const CategoryFailure.serverFailure();
    }
  }
}
