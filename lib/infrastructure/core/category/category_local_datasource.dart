import 'dart:convert';

import 'package:creonit_test_app/domain/category/category_failure.dart';
import 'package:creonit_test_app/infrastructure/category/category_model.dart';
import 'package:hive_flutter/hive_flutter.dart';

const String cachedCategory = 'category';

abstract class ICategoryLocalDatasource {
  Future<void> cacheCategory(List<CategoryModel> model);

  List<CategoryModel>? getCategoriesLocal();
}

class CategoryLocalDatasource implements ICategoryLocalDatasource {
  CategoryLocalDatasource({Box? box}) : _box = box;

  final Box? _box;

  @override
  Future<void> cacheCategory(List<CategoryModel> model) =>
      _box!.put(cachedCategory, json.encode(model));

  @override
  List<CategoryModel>? getCategoriesLocal() {
    try {
      final modelString = _box!.get(cachedCategory);
      if (modelString == null) {
        return null;
      }
      List<dynamic> decodeList = json.decode(modelString);
      var list = List<CategoryModel>.from(
          decodeList.map((e) => CategoryModel.fromJson(e)));
      return list;
    } on Exception catch (_) {
      throw const CategoryFailure.cacheFailure();
    }
  }
}
