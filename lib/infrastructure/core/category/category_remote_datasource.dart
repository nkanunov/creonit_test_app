import 'package:creonit_test_app/domain/category/category_failure.dart';
import 'package:creonit_test_app/infrastructure/category/category_model.dart';
import 'package:dio/dio.dart';

const categoryURL = 'https://vue-study.skillbox.cc/api/productCategories';

abstract class ICategoryRemoteDataSource {
  Future<List<CategoryModel>> getCategoriesFromServer();
}

const String cachedCategory = 'category';

class CategoryRemoteDataSource implements ICategoryRemoteDataSource {
  CategoryRemoteDataSource({required Dio client}) : _client = client;

  final Dio _client;

  @override
  Future<List<CategoryModel>> getCategoriesFromServer() async {
    try {
      final result = await _client.get(categoryURL);

      var list = List<CategoryModel>.from(
          (result.data['items']).map((e) => CategoryModel.fromJson(e)));

      return list;
    } on Exception catch (_) {
      throw const CategoryFailure.serverFailure();
    }
  }
}
