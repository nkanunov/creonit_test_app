import 'package:creonit_test_app/domain/category/entities/category.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'category_model.freezed.dart';

part 'category_model.g.dart';

@freezed
class CategoryModel with _$CategoryModel {
  factory CategoryModel.fromJson(Map<String, dynamic> json) =>
      _$CategoryModelFromJson(json);

  const factory CategoryModel(
      {required int id,
      required String title,
      required String slug}) = _CategoryModel;
}

extension CategoryModelxX on CategoryModel {
  Category toEntity() => Category(id: id, slug: slug, title: title);
}
