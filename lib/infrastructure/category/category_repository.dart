import 'package:creonit_test_app/domain/category/category_failure.dart';
import 'package:creonit_test_app/domain/category/entities/category.dart';
import 'package:creonit_test_app/domain/category/i_category_repository.dart';
import 'package:creonit_test_app/infrastructure/category/category_model.dart';
import 'package:creonit_test_app/infrastructure/core/category/category_local_datasource.dart';
import 'package:creonit_test_app/infrastructure/core/category/category_remote_datasource.dart';
import 'package:creonit_test_app/infrastructure/core/errors.dart';
import 'package:creonit_test_app/infrastructure/core/network_info.dart';
import 'package:dartz/dartz.dart';

class CategoryRepository implements ICategoryRepository {
  CategoryRepository({
    ICategoryLocalDatasource? categoryLocalDatasource,
    ICategoryRemoteDataSource? categoryRemoteDatasource,
    NetworkInfo? networkInfo,
  })  : _networkInfo = networkInfo,
        _categoryRemoteDatasource = categoryRemoteDatasource,
        _categoryLocalDatasource = categoryLocalDatasource;

  final ICategoryLocalDatasource? _categoryLocalDatasource;
  final NetworkInfo? _networkInfo;
  final ICategoryRemoteDataSource? _categoryRemoteDatasource;

  @override
  Future<Either<CategoryFailure, List<Category>?>?> getCategories() async {
    final _categories = await _getCategoryLocal();
    return _categories.fold(
        (exception) => const Left(CategoryFailure.unableToGetCategories()),
        (categories) async {
      if (categories == null) {
        return await _getCategoriesRemote();
      }
      return _categories;
    });
  }

  Future<Either<CategoryFailure, List<Category>>> _getCategoriesRemote() async {
    if (await _networkInfo!.isConnected) {
      try {
        final List<CategoryModel> model =
            await _categoryRemoteDatasource!.getCategoriesFromServer();
        List<Category> entities = [];
        for (var element in model) {
          entities.add(element.toEntity());
        }
        _categoryLocalDatasource!.cacheCategory(model);
        return Right(entities);
      } on ServerException {
        return const Left(CategoryFailure.unableToGetCategories());
      }
    }
    return const Left(CategoryFailure.serverFailure());
  }

  Future<Either<CategoryFailure, List<Category>?>> _getCategoryLocal() async {
    try {
      final List<CategoryModel>? model =
          _categoryLocalDatasource!.getCategoriesLocal();
      if (model == null) {
        return const Right(null);
      }
      List<Category> entities = [];
      for (var element in model) {
        entities.add(element.toEntity());
      }
      return Right(entities);
    } on CacheException {
      return const Left(CategoryFailure.unableToGetCategories());
    }
  }
}
