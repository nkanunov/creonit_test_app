import 'package:creonit_test_app/domain/product/entities/product.dart';
import 'package:creonit_test_app/domain/product/entities/server_image.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'product_model.freezed.dart';

part 'product_model.g.dart';

@freezed
class ProductModel with _$ProductModel {
  factory ProductModel.fromJson(Map<String, dynamic> json) =>
      _$ProductModelFromJson(json);

  const factory ProductModel(
      {required int id,
      required String title,
      required String slug,
      required ServerImage image,
      required int price}) = _ProductModel;
}

extension ProductModelX on ProductModel {
  Product toEntity() =>
      Product(id: id, title: title, slug: slug, image: image, price: price);
}
