import 'package:creonit_test_app/domain/category/category_failure.dart';
import 'package:creonit_test_app/domain/category/entities/category.dart';
import 'package:creonit_test_app/domain/category/i_category_repository.dart';
import 'package:creonit_test_app/domain/product/entities/product.dart';
import 'package:creonit_test_app/domain/product/i_product_repository.dart';
import 'package:creonit_test_app/domain/product/product_failure.dart';
import 'package:creonit_test_app/infrastructure/category/category_model.dart';
import 'package:creonit_test_app/infrastructure/core/category/category_local_datasource.dart';
import 'package:creonit_test_app/infrastructure/core/category/category_remote_datasource.dart';
import 'package:creonit_test_app/infrastructure/core/errors.dart';
import 'package:creonit_test_app/infrastructure/core/network_info.dart';
import 'package:creonit_test_app/infrastructure/core/product/product_remote_datasourse.dart';
import 'package:creonit_test_app/infrastructure/product/product_model.dart';
import 'package:dartz/dartz.dart';

class ProductRepository implements IProductRepository {
  ProductRepository({
    IProductsRemoteDataSource? productRemoteDatasource,
    NetworkInfo? networkInfo,
  })  : _networkInfo = networkInfo,
        _productRemoteDatasource = productRemoteDatasource;

  final NetworkInfo? _networkInfo;
  final IProductsRemoteDataSource? _productRemoteDatasource;

  @override
  Future<Either<ProductFailure, Tuple2<List<Product>, dynamic>?>?> getProducts(
      {required int index, required int page, int? limit}) async {
    final _products =
        await _getProductsRemote(index: index, page: page, limit: limit);
    return _products.fold(
        (exception) => const Left(ProductFailure.unableToGetProducts()),
        (products) => _products);
  }

  Future<Either<ProductFailure, Tuple2<List<Product>, dynamic>>>
      _getProductsRemote(
          {required int index, required int page, int? limit}) async {
    if (await _networkInfo!.isConnected) {
      try {
        final Tuple2<List<ProductModel>, dynamic> model =
            await _productRemoteDatasource!
                .getProductsFromServer(page: page, index: index, limit: limit);
        Tuple2<List<Product>, dynamic> entities =
            Tuple2<List<Product>, dynamic>([], model.value2);
        for (var element in model.value1) {
          entities.value1.add(element.toEntity());
        }

        return Right(entities);
      } on ServerException {
        return const Left(ProductFailure.unableToGetProducts());
      }
    }
    return const Left(ProductFailure.serverFailure());
  }

  @override
  Future<List<Product>> getCartRemote(String userAccessKey) async {
    if (await _networkInfo!.isConnected) {
      try {
        final List<ProductModel> model =
            await _productRemoteDatasource!.getCartFromServer(userAccessKey);
        List<Product> entities = [];
        for (var element in model) {
          entities.add(element.toEntity());
        }

        return entities;
      } on ServerException {
        throw const ProductFailure.unableToGetProducts();
      }
    }
    throw const ProductFailure.serverFailure();
  }

  @override
  Future<void> addProductToCard(int index, int quantity) async {
    if (await _networkInfo!.isConnected) {
      try {
        await _productRemoteDatasource!.addProductToCard(index, quantity);
      } on ServerException {
        throw const ProductFailure.serverFailure();
      }
    }
  }
}
