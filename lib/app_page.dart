import 'package:creonit_test_app/application/blocs/product_bloc/product_page_bloc.dart';
import 'package:creonit_test_app/application/core/MixinColor.dart';
import 'package:creonit_test_app/infrastructure/core/network_info.dart';
import 'package:creonit_test_app/infrastructure/core/product/product_remote_datasourse.dart';
import 'package:creonit_test_app/infrastructure/product/product_repository.dart';
import 'package:creonit_test_app/pages/category_page.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';

class AppPage extends StatefulWidget {
  const AppPage({Key? key}) : super(key: key);

  @override
  State<AppPage> createState() => _AppPageState();
}

class _AppPageState extends State<AppPage> with MixinColor {
  int _selectedIndex = 1;

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  final List<GlobalKey<NavigatorState>> _navigatorKeys = [
    GlobalKey<NavigatorState>(),
    GlobalKey<NavigatorState>(),
    GlobalKey<NavigatorState>(),
    GlobalKey<NavigatorState>(),
    GlobalKey<NavigatorState>(),
  ];

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (_) => ProductPageBloc(
            productRepository: ProductRepository(
              networkInfo: NetworkInfo(InternetConnectionChecker()),
              productRemoteDatasource: ProductsRemoteDataSource(client: Dio()),
            ),
          ),
        )
      ],
      child: WillPopScope(
        onWillPop: () async {
          final isFirstRouteInCurrentTab =
              !await _navigatorKeys[_selectedIndex].currentState!.maybePop();
          return isFirstRouteInCurrentTab;
        },
        child: Scaffold(
          body: IndexedStack(
            index: _selectedIndex,
            children: [
              _buildOffstageNavigator(0),
              _buildOffstageNavigator(1),
              _buildOffstageNavigator(2),
              _buildOffstageNavigator(3),
              _buildOffstageNavigator(4),
            ],
          ),
          bottomNavigationBar: BottomNavigationBar(
              unselectedItemColor: const Color(0xFF8A8884).withOpacity(0.56),
              showUnselectedLabels: true,
              backgroundColor: const Color(0xFF8A8884).withOpacity(0.56),
              items: [
                BottomNavigationBarItem(
                  icon: SvgPicture.asset(
                    'assets/icons/home.svg',
                    color: _selectedIndex == 0
                        ? activeIconBarColor()
                        : notActiveIconBarColor(),
                  ),
                  label: 'Главная',
                ),
                BottomNavigationBarItem(
                  icon: SvgPicture.asset(
                    'assets/icons/catalog_icon.svg',
                    color: _selectedIndex == 1
                        ? activeIconBarColor()
                        : notActiveIconBarColor(),
                  ),
                  label: 'Каталог',
                ),
                BottomNavigationBarItem(
                  icon: SvgPicture.asset(
                    'assets/icons/heart.svg',
                    color: _selectedIndex == 2
                        ? activeIconBarColor()
                        : notActiveIconBarColor(),
                  ),
                  label: 'Избранное',
                ),
                BottomNavigationBarItem(
                  icon: SvgPicture.asset(
                    'assets/icons/cart.svg',
                    color: _selectedIndex == 3
                        ? activeIconBarColor()
                        : notActiveIconBarColor(),
                  ),
                  label: 'Корзина',
                ),
                BottomNavigationBarItem(
                  icon: SvgPicture.asset(
                    'assets/icons/profile.svg',
                    color: _selectedIndex == 4
                        ? activeIconBarColor()
                        : notActiveIconBarColor(),
                  ),
                  label: 'Профиль',
                ),
              ],
              currentIndex: _selectedIndex,
              selectedItemColor: const Color(0xFF414951),
              onTap: _onItemTapped),
        ),
      ),
    );
  }

  Map<String, WidgetBuilder> _routeBuilders(BuildContext context, int index) {
    return {
      '/': (context) {
        return [
          const Center(
            child: Text(
              'Экран Главная в разработке',
            ),
          ),
          const CategoryPage(),
          const Center(
            child: Text(
              'Экран Избранное в разработке',
            ),
          ),
          const Center(
            child: Text(
              'Экран Корзина в разработке',
            ),
          ),
          const Center(
            child: Text(
              'Экран Профиль в разработке',
            ),
          ),
        ].elementAt(index);
      },
    };
  }

  Widget _buildOffstageNavigator(int index) {
    var routeBuilders = _routeBuilders(context, index);
    return Offstage(
      offstage: _selectedIndex != index,
      child: Navigator(
        key: _navigatorKeys[index],
        onGenerateRoute: (routeSettings) {
          return MaterialPageRoute(
            builder: (context) => routeBuilders[routeSettings.name]!(context),
          );
        },
      ),
    );
  }
}
