import 'package:flutter/material.dart';

mixin MixinColor {
  Color greyColor() => const Color(0xFF414951);

  Color dividerColor() => const Color(0xFF8A8884).withOpacity(0.24);

  Color productTitleBackgroundColor() =>
      const Color(0xFF8A8884).withOpacity(0.08);

  Color activeStateCardButtonColor() => const Color(0xFFFF0000);

  Color notActiveIconBarColor() => const Color(0xFF8A8884);

  Color activeIconBarColor() => const Color(0xFF414951);
}
