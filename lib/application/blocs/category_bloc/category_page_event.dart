part of 'category_page_bloc.dart';

@freezed
class CategoryPageEvent with _$CategoryPageEvent {
  const factory CategoryPageEvent.getCategories() =
  GetCategories;

}
