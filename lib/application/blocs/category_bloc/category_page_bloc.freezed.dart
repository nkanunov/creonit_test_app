// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'category_page_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$CategoryPageEventTearOff {
  const _$CategoryPageEventTearOff();

  GetCategories getCategories() {
    return const GetCategories();
  }
}

/// @nodoc
const $CategoryPageEvent = _$CategoryPageEventTearOff();

/// @nodoc
mixin _$CategoryPageEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() getCategories,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? getCategories,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? getCategories,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(GetCategories value) getCategories,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(GetCategories value)? getCategories,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(GetCategories value)? getCategories,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CategoryPageEventCopyWith<$Res> {
  factory $CategoryPageEventCopyWith(
          CategoryPageEvent value, $Res Function(CategoryPageEvent) then) =
      _$CategoryPageEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$CategoryPageEventCopyWithImpl<$Res>
    implements $CategoryPageEventCopyWith<$Res> {
  _$CategoryPageEventCopyWithImpl(this._value, this._then);

  final CategoryPageEvent _value;
  // ignore: unused_field
  final $Res Function(CategoryPageEvent) _then;
}

/// @nodoc
abstract class $GetCategoriesCopyWith<$Res> {
  factory $GetCategoriesCopyWith(
          GetCategories value, $Res Function(GetCategories) then) =
      _$GetCategoriesCopyWithImpl<$Res>;
}

/// @nodoc
class _$GetCategoriesCopyWithImpl<$Res>
    extends _$CategoryPageEventCopyWithImpl<$Res>
    implements $GetCategoriesCopyWith<$Res> {
  _$GetCategoriesCopyWithImpl(
      GetCategories _value, $Res Function(GetCategories) _then)
      : super(_value, (v) => _then(v as GetCategories));

  @override
  GetCategories get _value => super._value as GetCategories;
}

/// @nodoc

class _$GetCategories implements GetCategories {
  const _$GetCategories();

  @override
  String toString() {
    return 'CategoryPageEvent.getCategories()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is GetCategories);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() getCategories,
  }) {
    return getCategories();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? getCategories,
  }) {
    return getCategories?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? getCategories,
    required TResult orElse(),
  }) {
    if (getCategories != null) {
      return getCategories();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(GetCategories value) getCategories,
  }) {
    return getCategories(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(GetCategories value)? getCategories,
  }) {
    return getCategories?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(GetCategories value)? getCategories,
    required TResult orElse(),
  }) {
    if (getCategories != null) {
      return getCategories(this);
    }
    return orElse();
  }
}

abstract class GetCategories implements CategoryPageEvent {
  const factory GetCategories() = _$GetCategories;
}

/// @nodoc
class _$CategoryPageStateTearOff {
  const _$CategoryPageStateTearOff();

  InitialCategoryPageState initialState() {
    return const InitialCategoryPageState();
  }

  StateWithCategoryList stateWithCategoryList(List<Category> categories) {
    return StateWithCategoryList(
      categories,
    );
  }

  FailureState failureState() {
    return const FailureState();
  }
}

/// @nodoc
const $CategoryPageState = _$CategoryPageStateTearOff();

/// @nodoc
mixin _$CategoryPageState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initialState,
    required TResult Function(List<Category> categories) stateWithCategoryList,
    required TResult Function() failureState,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initialState,
    TResult Function(List<Category> categories)? stateWithCategoryList,
    TResult Function()? failureState,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initialState,
    TResult Function(List<Category> categories)? stateWithCategoryList,
    TResult Function()? failureState,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitialCategoryPageState value) initialState,
    required TResult Function(StateWithCategoryList value)
        stateWithCategoryList,
    required TResult Function(FailureState value) failureState,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitialCategoryPageState value)? initialState,
    TResult Function(StateWithCategoryList value)? stateWithCategoryList,
    TResult Function(FailureState value)? failureState,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitialCategoryPageState value)? initialState,
    TResult Function(StateWithCategoryList value)? stateWithCategoryList,
    TResult Function(FailureState value)? failureState,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CategoryPageStateCopyWith<$Res> {
  factory $CategoryPageStateCopyWith(
          CategoryPageState value, $Res Function(CategoryPageState) then) =
      _$CategoryPageStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$CategoryPageStateCopyWithImpl<$Res>
    implements $CategoryPageStateCopyWith<$Res> {
  _$CategoryPageStateCopyWithImpl(this._value, this._then);

  final CategoryPageState _value;
  // ignore: unused_field
  final $Res Function(CategoryPageState) _then;
}

/// @nodoc
abstract class $InitialCategoryPageStateCopyWith<$Res> {
  factory $InitialCategoryPageStateCopyWith(InitialCategoryPageState value,
          $Res Function(InitialCategoryPageState) then) =
      _$InitialCategoryPageStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$InitialCategoryPageStateCopyWithImpl<$Res>
    extends _$CategoryPageStateCopyWithImpl<$Res>
    implements $InitialCategoryPageStateCopyWith<$Res> {
  _$InitialCategoryPageStateCopyWithImpl(InitialCategoryPageState _value,
      $Res Function(InitialCategoryPageState) _then)
      : super(_value, (v) => _then(v as InitialCategoryPageState));

  @override
  InitialCategoryPageState get _value =>
      super._value as InitialCategoryPageState;
}

/// @nodoc

class _$InitialCategoryPageState implements InitialCategoryPageState {
  const _$InitialCategoryPageState();

  @override
  String toString() {
    return 'CategoryPageState.initialState()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is InitialCategoryPageState);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initialState,
    required TResult Function(List<Category> categories) stateWithCategoryList,
    required TResult Function() failureState,
  }) {
    return initialState();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initialState,
    TResult Function(List<Category> categories)? stateWithCategoryList,
    TResult Function()? failureState,
  }) {
    return initialState?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initialState,
    TResult Function(List<Category> categories)? stateWithCategoryList,
    TResult Function()? failureState,
    required TResult orElse(),
  }) {
    if (initialState != null) {
      return initialState();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitialCategoryPageState value) initialState,
    required TResult Function(StateWithCategoryList value)
        stateWithCategoryList,
    required TResult Function(FailureState value) failureState,
  }) {
    return initialState(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitialCategoryPageState value)? initialState,
    TResult Function(StateWithCategoryList value)? stateWithCategoryList,
    TResult Function(FailureState value)? failureState,
  }) {
    return initialState?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitialCategoryPageState value)? initialState,
    TResult Function(StateWithCategoryList value)? stateWithCategoryList,
    TResult Function(FailureState value)? failureState,
    required TResult orElse(),
  }) {
    if (initialState != null) {
      return initialState(this);
    }
    return orElse();
  }
}

abstract class InitialCategoryPageState implements CategoryPageState {
  const factory InitialCategoryPageState() = _$InitialCategoryPageState;
}

/// @nodoc
abstract class $StateWithCategoryListCopyWith<$Res> {
  factory $StateWithCategoryListCopyWith(StateWithCategoryList value,
          $Res Function(StateWithCategoryList) then) =
      _$StateWithCategoryListCopyWithImpl<$Res>;
  $Res call({List<Category> categories});
}

/// @nodoc
class _$StateWithCategoryListCopyWithImpl<$Res>
    extends _$CategoryPageStateCopyWithImpl<$Res>
    implements $StateWithCategoryListCopyWith<$Res> {
  _$StateWithCategoryListCopyWithImpl(
      StateWithCategoryList _value, $Res Function(StateWithCategoryList) _then)
      : super(_value, (v) => _then(v as StateWithCategoryList));

  @override
  StateWithCategoryList get _value => super._value as StateWithCategoryList;

  @override
  $Res call({
    Object? categories = freezed,
  }) {
    return _then(StateWithCategoryList(
      categories == freezed
          ? _value.categories
          : categories // ignore: cast_nullable_to_non_nullable
              as List<Category>,
    ));
  }
}

/// @nodoc

class _$StateWithCategoryList implements StateWithCategoryList {
  const _$StateWithCategoryList(this.categories);

  @override
  final List<Category> categories;

  @override
  String toString() {
    return 'CategoryPageState.stateWithCategoryList(categories: $categories)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is StateWithCategoryList &&
            const DeepCollectionEquality()
                .equals(other.categories, categories));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(categories));

  @JsonKey(ignore: true)
  @override
  $StateWithCategoryListCopyWith<StateWithCategoryList> get copyWith =>
      _$StateWithCategoryListCopyWithImpl<StateWithCategoryList>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initialState,
    required TResult Function(List<Category> categories) stateWithCategoryList,
    required TResult Function() failureState,
  }) {
    return stateWithCategoryList(categories);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initialState,
    TResult Function(List<Category> categories)? stateWithCategoryList,
    TResult Function()? failureState,
  }) {
    return stateWithCategoryList?.call(categories);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initialState,
    TResult Function(List<Category> categories)? stateWithCategoryList,
    TResult Function()? failureState,
    required TResult orElse(),
  }) {
    if (stateWithCategoryList != null) {
      return stateWithCategoryList(categories);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitialCategoryPageState value) initialState,
    required TResult Function(StateWithCategoryList value)
        stateWithCategoryList,
    required TResult Function(FailureState value) failureState,
  }) {
    return stateWithCategoryList(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitialCategoryPageState value)? initialState,
    TResult Function(StateWithCategoryList value)? stateWithCategoryList,
    TResult Function(FailureState value)? failureState,
  }) {
    return stateWithCategoryList?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitialCategoryPageState value)? initialState,
    TResult Function(StateWithCategoryList value)? stateWithCategoryList,
    TResult Function(FailureState value)? failureState,
    required TResult orElse(),
  }) {
    if (stateWithCategoryList != null) {
      return stateWithCategoryList(this);
    }
    return orElse();
  }
}

abstract class StateWithCategoryList implements CategoryPageState {
  const factory StateWithCategoryList(List<Category> categories) =
      _$StateWithCategoryList;

  List<Category> get categories;
  @JsonKey(ignore: true)
  $StateWithCategoryListCopyWith<StateWithCategoryList> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FailureStateCopyWith<$Res> {
  factory $FailureStateCopyWith(
          FailureState value, $Res Function(FailureState) then) =
      _$FailureStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$FailureStateCopyWithImpl<$Res>
    extends _$CategoryPageStateCopyWithImpl<$Res>
    implements $FailureStateCopyWith<$Res> {
  _$FailureStateCopyWithImpl(
      FailureState _value, $Res Function(FailureState) _then)
      : super(_value, (v) => _then(v as FailureState));

  @override
  FailureState get _value => super._value as FailureState;
}

/// @nodoc

class _$FailureState implements FailureState {
  const _$FailureState();

  @override
  String toString() {
    return 'CategoryPageState.failureState()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is FailureState);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initialState,
    required TResult Function(List<Category> categories) stateWithCategoryList,
    required TResult Function() failureState,
  }) {
    return failureState();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initialState,
    TResult Function(List<Category> categories)? stateWithCategoryList,
    TResult Function()? failureState,
  }) {
    return failureState?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initialState,
    TResult Function(List<Category> categories)? stateWithCategoryList,
    TResult Function()? failureState,
    required TResult orElse(),
  }) {
    if (failureState != null) {
      return failureState();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitialCategoryPageState value) initialState,
    required TResult Function(StateWithCategoryList value)
        stateWithCategoryList,
    required TResult Function(FailureState value) failureState,
  }) {
    return failureState(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitialCategoryPageState value)? initialState,
    TResult Function(StateWithCategoryList value)? stateWithCategoryList,
    TResult Function(FailureState value)? failureState,
  }) {
    return failureState?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitialCategoryPageState value)? initialState,
    TResult Function(StateWithCategoryList value)? stateWithCategoryList,
    TResult Function(FailureState value)? failureState,
    required TResult orElse(),
  }) {
    if (failureState != null) {
      return failureState(this);
    }
    return orElse();
  }
}

abstract class FailureState implements CategoryPageState {
  const factory FailureState() = _$FailureState;
}
