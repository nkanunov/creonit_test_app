part of 'category_page_bloc.dart';

@freezed
class CategoryPageState with _$CategoryPageState {
  const factory CategoryPageState.initialState() = InitialCategoryPageState;

  const factory CategoryPageState.stateWithCategoryList(List<Category> categories) =
      StateWithCategoryList;

  const factory CategoryPageState.failureState() =
  FailureState;
}
