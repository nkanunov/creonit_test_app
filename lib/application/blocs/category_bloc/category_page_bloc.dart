import 'package:creonit_test_app/domain/category/entities/category.dart';
import 'package:creonit_test_app/domain/category/i_category_repository.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'category_page_bloc.freezed.dart';

part 'category_page_event.dart';

part 'category_page_state.dart';

class CategoryPageBloc extends Bloc<CategoryPageEvent, CategoryPageState> {
  CategoryPageBloc({ICategoryRepository? categoryRepository})
      : _categoryRepository = categoryRepository,
        super(const CategoryPageState.initialState());

  final ICategoryRepository? _categoryRepository;

  @override
  Stream<CategoryPageState> mapEventToState(CategoryPageEvent event) async* {
    yield* event.map(
      getCategories: (value) async* {
        var _categories = await _categoryRepository!.getCategories();
        yield _categories!.fold(
          (_) => const CategoryPageState.failureState(),
          (categories) => CategoryPageState.stateWithCategoryList(categories!),
        );
      },
    );
  }
}
