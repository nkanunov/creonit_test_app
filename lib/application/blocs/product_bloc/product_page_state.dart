part of 'product_page_bloc.dart';

@freezed
class ProductPageState with _$ProductPageState {
  const factory ProductPageState.initialState() = InitialProductPageState;

  const factory ProductPageState.stateWithProductList(
          List<Product> products, List<bool> isProductAddedToCast, int pageCount) =
      StateWithProductList;

  const factory ProductPageState.failureState() = FailureState;
}
