// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'product_page_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$ProductPageEventTearOff {
  const _$ProductPageEventTearOff();

  GetProducts getProducts(
      {required int index,
      required int page,
      int? limit,
      List<bool>? isProductAddedToCast,
      required bool isFirst}) {
    return GetProducts(
      index: index,
      page: page,
      limit: limit,
      isProductAddedToCast: isProductAddedToCast,
      isFirst: isFirst,
    );
  }

  AddProductToCard addProductToCard(
      {required int id,
      required int quantity,
      List<bool>? isProductAddedToCast,
      int? index,
      List<Product>? products,
      required int pageCount}) {
    return AddProductToCard(
      id: id,
      quantity: quantity,
      isProductAddedToCast: isProductAddedToCast,
      index: index,
      products: products,
      pageCount: pageCount,
    );
  }

  ClearData clearData(
      {List<bool>? isProductAddedToCast, List<Product>? products}) {
    return ClearData(
      isProductAddedToCast: isProductAddedToCast,
      products: products,
    );
  }
}

/// @nodoc
const $ProductPageEvent = _$ProductPageEventTearOff();

/// @nodoc
mixin _$ProductPageEvent {
  List<bool>? get isProductAddedToCast => throw _privateConstructorUsedError;

  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(int index, int page, int? limit,
            List<bool>? isProductAddedToCast, bool isFirst)
        getProducts,
    required TResult Function(
            int id,
            int quantity,
            List<bool>? isProductAddedToCast,
            int? index,
            List<Product>? products,
            int pageCount)
        addProductToCard,
    required TResult Function(
            List<bool>? isProductAddedToCast, List<Product>? products)
        clearData,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(int index, int page, int? limit,
            List<bool>? isProductAddedToCast, bool isFirst)?
        getProducts,
    TResult Function(int id, int quantity, List<bool>? isProductAddedToCast,
            int? index, List<Product>? products, int pageCount)?
        addProductToCard,
    TResult Function(List<bool>? isProductAddedToCast, List<Product>? products)?
        clearData,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(int index, int page, int? limit,
            List<bool>? isProductAddedToCast, bool isFirst)?
        getProducts,
    TResult Function(int id, int quantity, List<bool>? isProductAddedToCast,
            int? index, List<Product>? products, int pageCount)?
        addProductToCard,
    TResult Function(List<bool>? isProductAddedToCast, List<Product>? products)?
        clearData,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(GetProducts value) getProducts,
    required TResult Function(AddProductToCard value) addProductToCard,
    required TResult Function(ClearData value) clearData,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(GetProducts value)? getProducts,
    TResult Function(AddProductToCard value)? addProductToCard,
    TResult Function(ClearData value)? clearData,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(GetProducts value)? getProducts,
    TResult Function(AddProductToCard value)? addProductToCard,
    TResult Function(ClearData value)? clearData,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $ProductPageEventCopyWith<ProductPageEvent> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ProductPageEventCopyWith<$Res> {
  factory $ProductPageEventCopyWith(
          ProductPageEvent value, $Res Function(ProductPageEvent) then) =
      _$ProductPageEventCopyWithImpl<$Res>;
  $Res call({List<bool>? isProductAddedToCast});
}

/// @nodoc
class _$ProductPageEventCopyWithImpl<$Res>
    implements $ProductPageEventCopyWith<$Res> {
  _$ProductPageEventCopyWithImpl(this._value, this._then);

  final ProductPageEvent _value;
  // ignore: unused_field
  final $Res Function(ProductPageEvent) _then;

  @override
  $Res call({
    Object? isProductAddedToCast = freezed,
  }) {
    return _then(_value.copyWith(
      isProductAddedToCast: isProductAddedToCast == freezed
          ? _value.isProductAddedToCast
          : isProductAddedToCast // ignore: cast_nullable_to_non_nullable
              as List<bool>?,
    ));
  }
}

/// @nodoc
abstract class $GetProductsCopyWith<$Res>
    implements $ProductPageEventCopyWith<$Res> {
  factory $GetProductsCopyWith(
          GetProducts value, $Res Function(GetProducts) then) =
      _$GetProductsCopyWithImpl<$Res>;
  @override
  $Res call(
      {int index,
      int page,
      int? limit,
      List<bool>? isProductAddedToCast,
      bool isFirst});
}

/// @nodoc
class _$GetProductsCopyWithImpl<$Res>
    extends _$ProductPageEventCopyWithImpl<$Res>
    implements $GetProductsCopyWith<$Res> {
  _$GetProductsCopyWithImpl(
      GetProducts _value, $Res Function(GetProducts) _then)
      : super(_value, (v) => _then(v as GetProducts));

  @override
  GetProducts get _value => super._value as GetProducts;

  @override
  $Res call({
    Object? index = freezed,
    Object? page = freezed,
    Object? limit = freezed,
    Object? isProductAddedToCast = freezed,
    Object? isFirst = freezed,
  }) {
    return _then(GetProducts(
      index: index == freezed
          ? _value.index
          : index // ignore: cast_nullable_to_non_nullable
              as int,
      page: page == freezed
          ? _value.page
          : page // ignore: cast_nullable_to_non_nullable
              as int,
      limit: limit == freezed
          ? _value.limit
          : limit // ignore: cast_nullable_to_non_nullable
              as int?,
      isProductAddedToCast: isProductAddedToCast == freezed
          ? _value.isProductAddedToCast
          : isProductAddedToCast // ignore: cast_nullable_to_non_nullable
              as List<bool>?,
      isFirst: isFirst == freezed
          ? _value.isFirst
          : isFirst // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$GetProducts implements GetProducts {
  const _$GetProducts(
      {required this.index,
      required this.page,
      this.limit,
      this.isProductAddedToCast,
      required this.isFirst});

  @override
  final int index;
  @override
  final int page;
  @override
  final int? limit;
  @override
  final List<bool>? isProductAddedToCast;
  @override
  final bool isFirst;

  @override
  String toString() {
    return 'ProductPageEvent.getProducts(index: $index, page: $page, limit: $limit, isProductAddedToCast: $isProductAddedToCast, isFirst: $isFirst)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is GetProducts &&
            (identical(other.index, index) || other.index == index) &&
            (identical(other.page, page) || other.page == page) &&
            (identical(other.limit, limit) || other.limit == limit) &&
            const DeepCollectionEquality()
                .equals(other.isProductAddedToCast, isProductAddedToCast) &&
            (identical(other.isFirst, isFirst) || other.isFirst == isFirst));
  }

  @override
  int get hashCode => Object.hash(runtimeType, index, page, limit,
      const DeepCollectionEquality().hash(isProductAddedToCast), isFirst);

  @JsonKey(ignore: true)
  @override
  $GetProductsCopyWith<GetProducts> get copyWith =>
      _$GetProductsCopyWithImpl<GetProducts>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(int index, int page, int? limit,
            List<bool>? isProductAddedToCast, bool isFirst)
        getProducts,
    required TResult Function(
            int id,
            int quantity,
            List<bool>? isProductAddedToCast,
            int? index,
            List<Product>? products,
            int pageCount)
        addProductToCard,
    required TResult Function(
            List<bool>? isProductAddedToCast, List<Product>? products)
        clearData,
  }) {
    return getProducts(index, page, limit, isProductAddedToCast, isFirst);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(int index, int page, int? limit,
            List<bool>? isProductAddedToCast, bool isFirst)?
        getProducts,
    TResult Function(int id, int quantity, List<bool>? isProductAddedToCast,
            int? index, List<Product>? products, int pageCount)?
        addProductToCard,
    TResult Function(List<bool>? isProductAddedToCast, List<Product>? products)?
        clearData,
  }) {
    return getProducts?.call(index, page, limit, isProductAddedToCast, isFirst);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(int index, int page, int? limit,
            List<bool>? isProductAddedToCast, bool isFirst)?
        getProducts,
    TResult Function(int id, int quantity, List<bool>? isProductAddedToCast,
            int? index, List<Product>? products, int pageCount)?
        addProductToCard,
    TResult Function(List<bool>? isProductAddedToCast, List<Product>? products)?
        clearData,
    required TResult orElse(),
  }) {
    if (getProducts != null) {
      return getProducts(index, page, limit, isProductAddedToCast, isFirst);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(GetProducts value) getProducts,
    required TResult Function(AddProductToCard value) addProductToCard,
    required TResult Function(ClearData value) clearData,
  }) {
    return getProducts(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(GetProducts value)? getProducts,
    TResult Function(AddProductToCard value)? addProductToCard,
    TResult Function(ClearData value)? clearData,
  }) {
    return getProducts?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(GetProducts value)? getProducts,
    TResult Function(AddProductToCard value)? addProductToCard,
    TResult Function(ClearData value)? clearData,
    required TResult orElse(),
  }) {
    if (getProducts != null) {
      return getProducts(this);
    }
    return orElse();
  }
}

abstract class GetProducts implements ProductPageEvent {
  const factory GetProducts(
      {required int index,
      required int page,
      int? limit,
      List<bool>? isProductAddedToCast,
      required bool isFirst}) = _$GetProducts;

  int get index;
  int get page;
  int? get limit;
  @override
  List<bool>? get isProductAddedToCast;
  bool get isFirst;
  @override
  @JsonKey(ignore: true)
  $GetProductsCopyWith<GetProducts> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AddProductToCardCopyWith<$Res>
    implements $ProductPageEventCopyWith<$Res> {
  factory $AddProductToCardCopyWith(
          AddProductToCard value, $Res Function(AddProductToCard) then) =
      _$AddProductToCardCopyWithImpl<$Res>;
  @override
  $Res call(
      {int id,
      int quantity,
      List<bool>? isProductAddedToCast,
      int? index,
      List<Product>? products,
      int pageCount});
}

/// @nodoc
class _$AddProductToCardCopyWithImpl<$Res>
    extends _$ProductPageEventCopyWithImpl<$Res>
    implements $AddProductToCardCopyWith<$Res> {
  _$AddProductToCardCopyWithImpl(
      AddProductToCard _value, $Res Function(AddProductToCard) _then)
      : super(_value, (v) => _then(v as AddProductToCard));

  @override
  AddProductToCard get _value => super._value as AddProductToCard;

  @override
  $Res call({
    Object? id = freezed,
    Object? quantity = freezed,
    Object? isProductAddedToCast = freezed,
    Object? index = freezed,
    Object? products = freezed,
    Object? pageCount = freezed,
  }) {
    return _then(AddProductToCard(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      quantity: quantity == freezed
          ? _value.quantity
          : quantity // ignore: cast_nullable_to_non_nullable
              as int,
      isProductAddedToCast: isProductAddedToCast == freezed
          ? _value.isProductAddedToCast
          : isProductAddedToCast // ignore: cast_nullable_to_non_nullable
              as List<bool>?,
      index: index == freezed
          ? _value.index
          : index // ignore: cast_nullable_to_non_nullable
              as int?,
      products: products == freezed
          ? _value.products
          : products // ignore: cast_nullable_to_non_nullable
              as List<Product>?,
      pageCount: pageCount == freezed
          ? _value.pageCount
          : pageCount // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$AddProductToCard implements AddProductToCard {
  const _$AddProductToCard(
      {required this.id,
      required this.quantity,
      this.isProductAddedToCast,
      this.index,
      this.products,
      required this.pageCount});

  @override
  final int id;
  @override
  final int quantity;
  @override
  final List<bool>? isProductAddedToCast;
  @override
  final int? index;
  @override
  final List<Product>? products;
  @override
  final int pageCount;

  @override
  String toString() {
    return 'ProductPageEvent.addProductToCard(id: $id, quantity: $quantity, isProductAddedToCast: $isProductAddedToCast, index: $index, products: $products, pageCount: $pageCount)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is AddProductToCard &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.quantity, quantity) ||
                other.quantity == quantity) &&
            const DeepCollectionEquality()
                .equals(other.isProductAddedToCast, isProductAddedToCast) &&
            (identical(other.index, index) || other.index == index) &&
            const DeepCollectionEquality().equals(other.products, products) &&
            (identical(other.pageCount, pageCount) ||
                other.pageCount == pageCount));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      id,
      quantity,
      const DeepCollectionEquality().hash(isProductAddedToCast),
      index,
      const DeepCollectionEquality().hash(products),
      pageCount);

  @JsonKey(ignore: true)
  @override
  $AddProductToCardCopyWith<AddProductToCard> get copyWith =>
      _$AddProductToCardCopyWithImpl<AddProductToCard>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(int index, int page, int? limit,
            List<bool>? isProductAddedToCast, bool isFirst)
        getProducts,
    required TResult Function(
            int id,
            int quantity,
            List<bool>? isProductAddedToCast,
            int? index,
            List<Product>? products,
            int pageCount)
        addProductToCard,
    required TResult Function(
            List<bool>? isProductAddedToCast, List<Product>? products)
        clearData,
  }) {
    return addProductToCard(
        id, quantity, isProductAddedToCast, index, products, pageCount);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(int index, int page, int? limit,
            List<bool>? isProductAddedToCast, bool isFirst)?
        getProducts,
    TResult Function(int id, int quantity, List<bool>? isProductAddedToCast,
            int? index, List<Product>? products, int pageCount)?
        addProductToCard,
    TResult Function(List<bool>? isProductAddedToCast, List<Product>? products)?
        clearData,
  }) {
    return addProductToCard?.call(
        id, quantity, isProductAddedToCast, index, products, pageCount);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(int index, int page, int? limit,
            List<bool>? isProductAddedToCast, bool isFirst)?
        getProducts,
    TResult Function(int id, int quantity, List<bool>? isProductAddedToCast,
            int? index, List<Product>? products, int pageCount)?
        addProductToCard,
    TResult Function(List<bool>? isProductAddedToCast, List<Product>? products)?
        clearData,
    required TResult orElse(),
  }) {
    if (addProductToCard != null) {
      return addProductToCard(
          id, quantity, isProductAddedToCast, index, products, pageCount);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(GetProducts value) getProducts,
    required TResult Function(AddProductToCard value) addProductToCard,
    required TResult Function(ClearData value) clearData,
  }) {
    return addProductToCard(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(GetProducts value)? getProducts,
    TResult Function(AddProductToCard value)? addProductToCard,
    TResult Function(ClearData value)? clearData,
  }) {
    return addProductToCard?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(GetProducts value)? getProducts,
    TResult Function(AddProductToCard value)? addProductToCard,
    TResult Function(ClearData value)? clearData,
    required TResult orElse(),
  }) {
    if (addProductToCard != null) {
      return addProductToCard(this);
    }
    return orElse();
  }
}

abstract class AddProductToCard implements ProductPageEvent {
  const factory AddProductToCard(
      {required int id,
      required int quantity,
      List<bool>? isProductAddedToCast,
      int? index,
      List<Product>? products,
      required int pageCount}) = _$AddProductToCard;

  int get id;
  int get quantity;
  @override
  List<bool>? get isProductAddedToCast;
  int? get index;
  List<Product>? get products;
  int get pageCount;
  @override
  @JsonKey(ignore: true)
  $AddProductToCardCopyWith<AddProductToCard> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ClearDataCopyWith<$Res>
    implements $ProductPageEventCopyWith<$Res> {
  factory $ClearDataCopyWith(ClearData value, $Res Function(ClearData) then) =
      _$ClearDataCopyWithImpl<$Res>;
  @override
  $Res call({List<bool>? isProductAddedToCast, List<Product>? products});
}

/// @nodoc
class _$ClearDataCopyWithImpl<$Res> extends _$ProductPageEventCopyWithImpl<$Res>
    implements $ClearDataCopyWith<$Res> {
  _$ClearDataCopyWithImpl(ClearData _value, $Res Function(ClearData) _then)
      : super(_value, (v) => _then(v as ClearData));

  @override
  ClearData get _value => super._value as ClearData;

  @override
  $Res call({
    Object? isProductAddedToCast = freezed,
    Object? products = freezed,
  }) {
    return _then(ClearData(
      isProductAddedToCast: isProductAddedToCast == freezed
          ? _value.isProductAddedToCast
          : isProductAddedToCast // ignore: cast_nullable_to_non_nullable
              as List<bool>?,
      products: products == freezed
          ? _value.products
          : products // ignore: cast_nullable_to_non_nullable
              as List<Product>?,
    ));
  }
}

/// @nodoc

class _$ClearData implements ClearData {
  const _$ClearData({this.isProductAddedToCast, this.products});

  @override
  final List<bool>? isProductAddedToCast;
  @override
  final List<Product>? products;

  @override
  String toString() {
    return 'ProductPageEvent.clearData(isProductAddedToCast: $isProductAddedToCast, products: $products)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is ClearData &&
            const DeepCollectionEquality()
                .equals(other.isProductAddedToCast, isProductAddedToCast) &&
            const DeepCollectionEquality().equals(other.products, products));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(isProductAddedToCast),
      const DeepCollectionEquality().hash(products));

  @JsonKey(ignore: true)
  @override
  $ClearDataCopyWith<ClearData> get copyWith =>
      _$ClearDataCopyWithImpl<ClearData>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(int index, int page, int? limit,
            List<bool>? isProductAddedToCast, bool isFirst)
        getProducts,
    required TResult Function(
            int id,
            int quantity,
            List<bool>? isProductAddedToCast,
            int? index,
            List<Product>? products,
            int pageCount)
        addProductToCard,
    required TResult Function(
            List<bool>? isProductAddedToCast, List<Product>? products)
        clearData,
  }) {
    return clearData(isProductAddedToCast, products);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(int index, int page, int? limit,
            List<bool>? isProductAddedToCast, bool isFirst)?
        getProducts,
    TResult Function(int id, int quantity, List<bool>? isProductAddedToCast,
            int? index, List<Product>? products, int pageCount)?
        addProductToCard,
    TResult Function(List<bool>? isProductAddedToCast, List<Product>? products)?
        clearData,
  }) {
    return clearData?.call(isProductAddedToCast, products);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(int index, int page, int? limit,
            List<bool>? isProductAddedToCast, bool isFirst)?
        getProducts,
    TResult Function(int id, int quantity, List<bool>? isProductAddedToCast,
            int? index, List<Product>? products, int pageCount)?
        addProductToCard,
    TResult Function(List<bool>? isProductAddedToCast, List<Product>? products)?
        clearData,
    required TResult orElse(),
  }) {
    if (clearData != null) {
      return clearData(isProductAddedToCast, products);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(GetProducts value) getProducts,
    required TResult Function(AddProductToCard value) addProductToCard,
    required TResult Function(ClearData value) clearData,
  }) {
    return clearData(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(GetProducts value)? getProducts,
    TResult Function(AddProductToCard value)? addProductToCard,
    TResult Function(ClearData value)? clearData,
  }) {
    return clearData?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(GetProducts value)? getProducts,
    TResult Function(AddProductToCard value)? addProductToCard,
    TResult Function(ClearData value)? clearData,
    required TResult orElse(),
  }) {
    if (clearData != null) {
      return clearData(this);
    }
    return orElse();
  }
}

abstract class ClearData implements ProductPageEvent {
  const factory ClearData(
      {List<bool>? isProductAddedToCast,
      List<Product>? products}) = _$ClearData;

  @override
  List<bool>? get isProductAddedToCast;
  List<Product>? get products;
  @override
  @JsonKey(ignore: true)
  $ClearDataCopyWith<ClearData> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
class _$ProductPageStateTearOff {
  const _$ProductPageStateTearOff();

  InitialProductPageState initialState() {
    return const InitialProductPageState();
  }

  StateWithProductList stateWithProductList(
      List<Product> products, List<bool> isProductAddedToCast, int pageCount) {
    return StateWithProductList(
      products,
      isProductAddedToCast,
      pageCount,
    );
  }

  FailureState failureState() {
    return const FailureState();
  }
}

/// @nodoc
const $ProductPageState = _$ProductPageStateTearOff();

/// @nodoc
mixin _$ProductPageState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initialState,
    required TResult Function(List<Product> products,
            List<bool> isProductAddedToCast, int pageCount)
        stateWithProductList,
    required TResult Function() failureState,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initialState,
    TResult Function(List<Product> products, List<bool> isProductAddedToCast,
            int pageCount)?
        stateWithProductList,
    TResult Function()? failureState,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initialState,
    TResult Function(List<Product> products, List<bool> isProductAddedToCast,
            int pageCount)?
        stateWithProductList,
    TResult Function()? failureState,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitialProductPageState value) initialState,
    required TResult Function(StateWithProductList value) stateWithProductList,
    required TResult Function(FailureState value) failureState,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitialProductPageState value)? initialState,
    TResult Function(StateWithProductList value)? stateWithProductList,
    TResult Function(FailureState value)? failureState,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitialProductPageState value)? initialState,
    TResult Function(StateWithProductList value)? stateWithProductList,
    TResult Function(FailureState value)? failureState,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ProductPageStateCopyWith<$Res> {
  factory $ProductPageStateCopyWith(
          ProductPageState value, $Res Function(ProductPageState) then) =
      _$ProductPageStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$ProductPageStateCopyWithImpl<$Res>
    implements $ProductPageStateCopyWith<$Res> {
  _$ProductPageStateCopyWithImpl(this._value, this._then);

  final ProductPageState _value;
  // ignore: unused_field
  final $Res Function(ProductPageState) _then;
}

/// @nodoc
abstract class $InitialProductPageStateCopyWith<$Res> {
  factory $InitialProductPageStateCopyWith(InitialProductPageState value,
          $Res Function(InitialProductPageState) then) =
      _$InitialProductPageStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$InitialProductPageStateCopyWithImpl<$Res>
    extends _$ProductPageStateCopyWithImpl<$Res>
    implements $InitialProductPageStateCopyWith<$Res> {
  _$InitialProductPageStateCopyWithImpl(InitialProductPageState _value,
      $Res Function(InitialProductPageState) _then)
      : super(_value, (v) => _then(v as InitialProductPageState));

  @override
  InitialProductPageState get _value => super._value as InitialProductPageState;
}

/// @nodoc

class _$InitialProductPageState implements InitialProductPageState {
  const _$InitialProductPageState();

  @override
  String toString() {
    return 'ProductPageState.initialState()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is InitialProductPageState);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initialState,
    required TResult Function(List<Product> products,
            List<bool> isProductAddedToCast, int pageCount)
        stateWithProductList,
    required TResult Function() failureState,
  }) {
    return initialState();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initialState,
    TResult Function(List<Product> products, List<bool> isProductAddedToCast,
            int pageCount)?
        stateWithProductList,
    TResult Function()? failureState,
  }) {
    return initialState?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initialState,
    TResult Function(List<Product> products, List<bool> isProductAddedToCast,
            int pageCount)?
        stateWithProductList,
    TResult Function()? failureState,
    required TResult orElse(),
  }) {
    if (initialState != null) {
      return initialState();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitialProductPageState value) initialState,
    required TResult Function(StateWithProductList value) stateWithProductList,
    required TResult Function(FailureState value) failureState,
  }) {
    return initialState(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitialProductPageState value)? initialState,
    TResult Function(StateWithProductList value)? stateWithProductList,
    TResult Function(FailureState value)? failureState,
  }) {
    return initialState?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitialProductPageState value)? initialState,
    TResult Function(StateWithProductList value)? stateWithProductList,
    TResult Function(FailureState value)? failureState,
    required TResult orElse(),
  }) {
    if (initialState != null) {
      return initialState(this);
    }
    return orElse();
  }
}

abstract class InitialProductPageState implements ProductPageState {
  const factory InitialProductPageState() = _$InitialProductPageState;
}

/// @nodoc
abstract class $StateWithProductListCopyWith<$Res> {
  factory $StateWithProductListCopyWith(StateWithProductList value,
          $Res Function(StateWithProductList) then) =
      _$StateWithProductListCopyWithImpl<$Res>;
  $Res call(
      {List<Product> products, List<bool> isProductAddedToCast, int pageCount});
}

/// @nodoc
class _$StateWithProductListCopyWithImpl<$Res>
    extends _$ProductPageStateCopyWithImpl<$Res>
    implements $StateWithProductListCopyWith<$Res> {
  _$StateWithProductListCopyWithImpl(
      StateWithProductList _value, $Res Function(StateWithProductList) _then)
      : super(_value, (v) => _then(v as StateWithProductList));

  @override
  StateWithProductList get _value => super._value as StateWithProductList;

  @override
  $Res call({
    Object? products = freezed,
    Object? isProductAddedToCast = freezed,
    Object? pageCount = freezed,
  }) {
    return _then(StateWithProductList(
      products == freezed
          ? _value.products
          : products // ignore: cast_nullable_to_non_nullable
              as List<Product>,
      isProductAddedToCast == freezed
          ? _value.isProductAddedToCast
          : isProductAddedToCast // ignore: cast_nullable_to_non_nullable
              as List<bool>,
      pageCount == freezed
          ? _value.pageCount
          : pageCount // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$StateWithProductList implements StateWithProductList {
  const _$StateWithProductList(
      this.products, this.isProductAddedToCast, this.pageCount);

  @override
  final List<Product> products;
  @override
  final List<bool> isProductAddedToCast;
  @override
  final int pageCount;

  @override
  String toString() {
    return 'ProductPageState.stateWithProductList(products: $products, isProductAddedToCast: $isProductAddedToCast, pageCount: $pageCount)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is StateWithProductList &&
            const DeepCollectionEquality().equals(other.products, products) &&
            const DeepCollectionEquality()
                .equals(other.isProductAddedToCast, isProductAddedToCast) &&
            (identical(other.pageCount, pageCount) ||
                other.pageCount == pageCount));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(products),
      const DeepCollectionEquality().hash(isProductAddedToCast),
      pageCount);

  @JsonKey(ignore: true)
  @override
  $StateWithProductListCopyWith<StateWithProductList> get copyWith =>
      _$StateWithProductListCopyWithImpl<StateWithProductList>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initialState,
    required TResult Function(List<Product> products,
            List<bool> isProductAddedToCast, int pageCount)
        stateWithProductList,
    required TResult Function() failureState,
  }) {
    return stateWithProductList(products, isProductAddedToCast, pageCount);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initialState,
    TResult Function(List<Product> products, List<bool> isProductAddedToCast,
            int pageCount)?
        stateWithProductList,
    TResult Function()? failureState,
  }) {
    return stateWithProductList?.call(
        products, isProductAddedToCast, pageCount);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initialState,
    TResult Function(List<Product> products, List<bool> isProductAddedToCast,
            int pageCount)?
        stateWithProductList,
    TResult Function()? failureState,
    required TResult orElse(),
  }) {
    if (stateWithProductList != null) {
      return stateWithProductList(products, isProductAddedToCast, pageCount);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitialProductPageState value) initialState,
    required TResult Function(StateWithProductList value) stateWithProductList,
    required TResult Function(FailureState value) failureState,
  }) {
    return stateWithProductList(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitialProductPageState value)? initialState,
    TResult Function(StateWithProductList value)? stateWithProductList,
    TResult Function(FailureState value)? failureState,
  }) {
    return stateWithProductList?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitialProductPageState value)? initialState,
    TResult Function(StateWithProductList value)? stateWithProductList,
    TResult Function(FailureState value)? failureState,
    required TResult orElse(),
  }) {
    if (stateWithProductList != null) {
      return stateWithProductList(this);
    }
    return orElse();
  }
}

abstract class StateWithProductList implements ProductPageState {
  const factory StateWithProductList(List<Product> products,
      List<bool> isProductAddedToCast, int pageCount) = _$StateWithProductList;

  List<Product> get products;
  List<bool> get isProductAddedToCast;
  int get pageCount;
  @JsonKey(ignore: true)
  $StateWithProductListCopyWith<StateWithProductList> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FailureStateCopyWith<$Res> {
  factory $FailureStateCopyWith(
          FailureState value, $Res Function(FailureState) then) =
      _$FailureStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$FailureStateCopyWithImpl<$Res>
    extends _$ProductPageStateCopyWithImpl<$Res>
    implements $FailureStateCopyWith<$Res> {
  _$FailureStateCopyWithImpl(
      FailureState _value, $Res Function(FailureState) _then)
      : super(_value, (v) => _then(v as FailureState));

  @override
  FailureState get _value => super._value as FailureState;
}

/// @nodoc

class _$FailureState implements FailureState {
  const _$FailureState();

  @override
  String toString() {
    return 'ProductPageState.failureState()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is FailureState);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initialState,
    required TResult Function(List<Product> products,
            List<bool> isProductAddedToCast, int pageCount)
        stateWithProductList,
    required TResult Function() failureState,
  }) {
    return failureState();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initialState,
    TResult Function(List<Product> products, List<bool> isProductAddedToCast,
            int pageCount)?
        stateWithProductList,
    TResult Function()? failureState,
  }) {
    return failureState?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initialState,
    TResult Function(List<Product> products, List<bool> isProductAddedToCast,
            int pageCount)?
        stateWithProductList,
    TResult Function()? failureState,
    required TResult orElse(),
  }) {
    if (failureState != null) {
      return failureState();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitialProductPageState value) initialState,
    required TResult Function(StateWithProductList value) stateWithProductList,
    required TResult Function(FailureState value) failureState,
  }) {
    return failureState(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitialProductPageState value)? initialState,
    TResult Function(StateWithProductList value)? stateWithProductList,
    TResult Function(FailureState value)? failureState,
  }) {
    return failureState?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitialProductPageState value)? initialState,
    TResult Function(StateWithProductList value)? stateWithProductList,
    TResult Function(FailureState value)? failureState,
    required TResult orElse(),
  }) {
    if (failureState != null) {
      return failureState(this);
    }
    return orElse();
  }
}

abstract class FailureState implements ProductPageState {
  const factory FailureState() = _$FailureState;
}
