import 'package:creonit_test_app/domain/product/entities/product.dart';
import 'package:creonit_test_app/domain/product/i_product_repository.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'product_page_bloc.freezed.dart';

part 'product_page_event.dart';

part 'product_page_state.dart';

class ProductPageBloc extends Bloc<ProductPageEvent, ProductPageState> {
  ProductPageBloc({IProductRepository? productRepository})
      : _productRepository = productRepository,
        super(const ProductPageState.initialState());

  final IProductRepository? _productRepository;

  static List<Product> _listProducts = [];
  List<bool> isProduct = [];
  List<Product> cartList = [];

  @override
  Stream<ProductPageState> mapEventToState(ProductPageEvent event) async* {
    yield* event.map(
      getProducts: (value) async* {
        SharedPreferences _preferences = await SharedPreferences.getInstance();
        var accessKey = _preferences.getString('userAccessKey');
        if (accessKey != null) {
          cartList = await _productRepository!.getCartRemote(accessKey);
        }
        var _products = await _productRepository!.getProducts(
            index: value.index, page: value.page, limit: value.limit);
        yield _products!.fold((_) => const ProductPageState.failureState(),
            (products) {
          for (var element in products!.value1) {
            _listProducts.add(element);
          }

          isProduct = List.generate(_listProducts.length, (index) => false);

          if (accessKey != null) {
            for (var i = 0; i < cartList.length; i++) {
              for (var j = 0; j < products.value1.length; j++) {
                if (products.value1[j].title.contains(cartList[i].title)) {
                  isProduct[j] = true;
                }
              }
            }
          }
          for (var i = 0; i < value.isProductAddedToCast!.length; i++) {
            isProduct[i] = value.isProductAddedToCast![i];
          }
          return ProductPageState.stateWithProductList(
              _listProducts, isProduct, products.value2);
        });
      },
      addProductToCard: (value) async* {
        value.isProductAddedToCast![value.index!] = true;
        yield ProductPageState.stateWithProductList(
            value.products!, value.isProductAddedToCast!, value.pageCount);
        await _productRepository!.addProductToCard(value.id, value.quantity);
      },
      clearData: (value) async* {
        value.products!.clear();
        value.isProductAddedToCast!.clear();
        yield ProductPageState.stateWithProductList(
            value.products!, value.isProductAddedToCast!, 0);
      },
    );
  }
}
