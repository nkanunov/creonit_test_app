part of 'product_page_bloc.dart';

@freezed
class ProductPageEvent with _$ProductPageEvent {
  const factory ProductPageEvent.getProducts({
    required int index,
    required int page,
    int? limit,
    List<bool>? isProductAddedToCast,
    required bool isFirst,
  }) = GetProducts;

  const factory ProductPageEvent.addProductToCard(
      {required int id,
      required int quantity,
      List<bool>? isProductAddedToCast,
      int? index,
      List<Product>? products,
      required int pageCount}) = AddProductToCard;

  const factory ProductPageEvent.clearData({ List<bool>? isProductAddedToCast,
      List<Product>? products}) = ClearData;
}
