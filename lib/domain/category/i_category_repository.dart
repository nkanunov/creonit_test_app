import 'package:creonit_test_app/domain/category/category_failure.dart';
import 'package:creonit_test_app/domain/category/entities/category.dart';
import 'package:dartz/dartz.dart';

abstract class ICategoryRepository {
  Future<Either<CategoryFailure, List<Category>?>?> getCategories();
}
