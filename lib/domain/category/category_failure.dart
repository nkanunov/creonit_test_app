import 'package:freezed_annotation/freezed_annotation.dart';

part 'category_failure.freezed.dart';

@freezed
class CategoryFailure with _$CategoryFailure {
  const factory CategoryFailure.serverFailure() = ServerFailure;
  const factory CategoryFailure.cacheFailure() = CacheFailure;

  const factory CategoryFailure.unableToGetCategories() = UnableToGetCategories;
}
