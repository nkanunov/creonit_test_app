// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'server_image.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_ServerImage _$$_ServerImageFromJson(Map<String, dynamic> json) =>
    _$_ServerImage(
      file: ServerFile.fromJson(json['file'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$$_ServerImageToJson(_$_ServerImage instance) =>
    <String, dynamic>{
      'file': instance.file,
    };
