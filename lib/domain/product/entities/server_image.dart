import 'package:creonit_test_app/domain/product/entities/server_file.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'server_image.freezed.dart';

part 'server_image.g.dart';

@freezed
class ServerImage with _$ServerImage {
  const factory ServerImage({
    required ServerFile file,

  }) = _ServerImage;

  factory ServerImage.fromJson(Map<String, dynamic> json) =>
      _$ServerImageFromJson(json);
}
