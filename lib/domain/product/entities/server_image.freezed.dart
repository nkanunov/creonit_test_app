// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'server_image.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

ServerImage _$ServerImageFromJson(Map<String, dynamic> json) {
  return _ServerImage.fromJson(json);
}

/// @nodoc
class _$ServerImageTearOff {
  const _$ServerImageTearOff();

  _ServerImage call({required ServerFile file}) {
    return _ServerImage(
      file: file,
    );
  }

  ServerImage fromJson(Map<String, Object?> json) {
    return ServerImage.fromJson(json);
  }
}

/// @nodoc
const $ServerImage = _$ServerImageTearOff();

/// @nodoc
mixin _$ServerImage {
  ServerFile get file => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ServerImageCopyWith<ServerImage> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ServerImageCopyWith<$Res> {
  factory $ServerImageCopyWith(
          ServerImage value, $Res Function(ServerImage) then) =
      _$ServerImageCopyWithImpl<$Res>;
  $Res call({ServerFile file});

  $ServerFileCopyWith<$Res> get file;
}

/// @nodoc
class _$ServerImageCopyWithImpl<$Res> implements $ServerImageCopyWith<$Res> {
  _$ServerImageCopyWithImpl(this._value, this._then);

  final ServerImage _value;
  // ignore: unused_field
  final $Res Function(ServerImage) _then;

  @override
  $Res call({
    Object? file = freezed,
  }) {
    return _then(_value.copyWith(
      file: file == freezed
          ? _value.file
          : file // ignore: cast_nullable_to_non_nullable
              as ServerFile,
    ));
  }

  @override
  $ServerFileCopyWith<$Res> get file {
    return $ServerFileCopyWith<$Res>(_value.file, (value) {
      return _then(_value.copyWith(file: value));
    });
  }
}

/// @nodoc
abstract class _$ServerImageCopyWith<$Res>
    implements $ServerImageCopyWith<$Res> {
  factory _$ServerImageCopyWith(
          _ServerImage value, $Res Function(_ServerImage) then) =
      __$ServerImageCopyWithImpl<$Res>;
  @override
  $Res call({ServerFile file});

  @override
  $ServerFileCopyWith<$Res> get file;
}

/// @nodoc
class __$ServerImageCopyWithImpl<$Res> extends _$ServerImageCopyWithImpl<$Res>
    implements _$ServerImageCopyWith<$Res> {
  __$ServerImageCopyWithImpl(
      _ServerImage _value, $Res Function(_ServerImage) _then)
      : super(_value, (v) => _then(v as _ServerImage));

  @override
  _ServerImage get _value => super._value as _ServerImage;

  @override
  $Res call({
    Object? file = freezed,
  }) {
    return _then(_ServerImage(
      file: file == freezed
          ? _value.file
          : file // ignore: cast_nullable_to_non_nullable
              as ServerFile,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_ServerImage implements _ServerImage {
  const _$_ServerImage({required this.file});

  factory _$_ServerImage.fromJson(Map<String, dynamic> json) =>
      _$$_ServerImageFromJson(json);

  @override
  final ServerFile file;

  @override
  String toString() {
    return 'ServerImage(file: $file)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _ServerImage &&
            (identical(other.file, file) || other.file == file));
  }

  @override
  int get hashCode => Object.hash(runtimeType, file);

  @JsonKey(ignore: true)
  @override
  _$ServerImageCopyWith<_ServerImage> get copyWith =>
      __$ServerImageCopyWithImpl<_ServerImage>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_ServerImageToJson(this);
  }
}

abstract class _ServerImage implements ServerImage {
  const factory _ServerImage({required ServerFile file}) = _$_ServerImage;

  factory _ServerImage.fromJson(Map<String, dynamic> json) =
      _$_ServerImage.fromJson;

  @override
  ServerFile get file;
  @override
  @JsonKey(ignore: true)
  _$ServerImageCopyWith<_ServerImage> get copyWith =>
      throw _privateConstructorUsedError;
}
