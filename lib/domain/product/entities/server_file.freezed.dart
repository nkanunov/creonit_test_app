// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'server_file.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

ServerFile _$ServerFileFromJson(Map<String, dynamic> json) {
  return _ServerFile.fromJson(json);
}

/// @nodoc
class _$ServerFileTearOff {
  const _$ServerFileTearOff();

  _ServerFile call({required String url}) {
    return _ServerFile(
      url: url,
    );
  }

  ServerFile fromJson(Map<String, Object?> json) {
    return ServerFile.fromJson(json);
  }
}

/// @nodoc
const $ServerFile = _$ServerFileTearOff();

/// @nodoc
mixin _$ServerFile {
  String get url => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ServerFileCopyWith<ServerFile> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ServerFileCopyWith<$Res> {
  factory $ServerFileCopyWith(
          ServerFile value, $Res Function(ServerFile) then) =
      _$ServerFileCopyWithImpl<$Res>;
  $Res call({String url});
}

/// @nodoc
class _$ServerFileCopyWithImpl<$Res> implements $ServerFileCopyWith<$Res> {
  _$ServerFileCopyWithImpl(this._value, this._then);

  final ServerFile _value;
  // ignore: unused_field
  final $Res Function(ServerFile) _then;

  @override
  $Res call({
    Object? url = freezed,
  }) {
    return _then(_value.copyWith(
      url: url == freezed
          ? _value.url
          : url // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
abstract class _$ServerFileCopyWith<$Res> implements $ServerFileCopyWith<$Res> {
  factory _$ServerFileCopyWith(
          _ServerFile value, $Res Function(_ServerFile) then) =
      __$ServerFileCopyWithImpl<$Res>;
  @override
  $Res call({String url});
}

/// @nodoc
class __$ServerFileCopyWithImpl<$Res> extends _$ServerFileCopyWithImpl<$Res>
    implements _$ServerFileCopyWith<$Res> {
  __$ServerFileCopyWithImpl(
      _ServerFile _value, $Res Function(_ServerFile) _then)
      : super(_value, (v) => _then(v as _ServerFile));

  @override
  _ServerFile get _value => super._value as _ServerFile;

  @override
  $Res call({
    Object? url = freezed,
  }) {
    return _then(_ServerFile(
      url: url == freezed
          ? _value.url
          : url // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_ServerFile implements _ServerFile {
  const _$_ServerFile({required this.url});

  factory _$_ServerFile.fromJson(Map<String, dynamic> json) =>
      _$$_ServerFileFromJson(json);

  @override
  final String url;

  @override
  String toString() {
    return 'ServerFile(url: $url)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _ServerFile &&
            (identical(other.url, url) || other.url == url));
  }

  @override
  int get hashCode => Object.hash(runtimeType, url);

  @JsonKey(ignore: true)
  @override
  _$ServerFileCopyWith<_ServerFile> get copyWith =>
      __$ServerFileCopyWithImpl<_ServerFile>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_ServerFileToJson(this);
  }
}

abstract class _ServerFile implements ServerFile {
  const factory _ServerFile({required String url}) = _$_ServerFile;

  factory _ServerFile.fromJson(Map<String, dynamic> json) =
      _$_ServerFile.fromJson;

  @override
  String get url;
  @override
  @JsonKey(ignore: true)
  _$ServerFileCopyWith<_ServerFile> get copyWith =>
      throw _privateConstructorUsedError;
}
