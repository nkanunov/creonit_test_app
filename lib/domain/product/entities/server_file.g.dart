// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'server_file.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_ServerFile _$$_ServerFileFromJson(Map<String, dynamic> json) =>
    _$_ServerFile(
      url: json['url'] as String,
    );

Map<String, dynamic> _$$_ServerFileToJson(_$_ServerFile instance) =>
    <String, dynamic>{
      'url': instance.url,
    };
