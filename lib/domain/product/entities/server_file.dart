import 'package:freezed_annotation/freezed_annotation.dart';

part 'server_file.freezed.dart';

part 'server_file.g.dart';

@freezed
class ServerFile with _$ServerFile {
  const factory ServerFile({
    required String url,
  }) = _ServerFile;

  factory ServerFile.fromJson(Map<String, dynamic> json) =>
      _$ServerFileFromJson(json);
}
