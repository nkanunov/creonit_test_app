import 'package:creonit_test_app/domain/product/entities/product.dart';
import 'package:creonit_test_app/domain/product/product_failure.dart';
import 'package:dartz/dartz.dart';

abstract class IProductRepository {
  Future<Either<ProductFailure, Tuple2<List<Product>, dynamic>?>?> getProducts({required int index, required int page, int? limit});
  Future<void> addProductToCard(int index, int quantity);
  Future<List<Product>> getCartRemote(String userAccessKey);
}
