import 'package:freezed_annotation/freezed_annotation.dart';

part 'product_failure.freezed.dart';

@freezed
class ProductFailure with _$ProductFailure {
  const factory ProductFailure.serverFailure() = ServerFailure;

  const factory ProductFailure.cacheFailure() = CacheFailure;

  const factory ProductFailure.unableToGetProducts() = UnableToGetProducts;
}
